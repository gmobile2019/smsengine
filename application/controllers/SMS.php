<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of SMS
 *
 * @author Manuel
 */
class SMS extends CI_Controller{
    
    function __construct() {
        parent::__construct();
       
        $this->load->model('SMS_model');
        ini_set('max_execution_time',0);
    }
    
/*************************************************infobip sms functions (route1)*******************************************************************/
    
    /*
     * name : route1
     * parameters : id (processing instance)
     * description : infobip route processing function
    */
    function route1($id){
        $lock =file_exists('./locks/route1_'.$id.'.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/route1_'.$id.'.lock','w'); 
        }
        
        $i=0;
        while($i++ <=$this->config->item('loopcount')){
            $destination="./logs/route1_".date('Y-m-d').'.log';
            $logfile=file_exists($destination);

            if($logfile){

                $filesize=filesize($destination);

                if($filesize > 1048576){
                    rename($destination,"./logs/route1_".date('His')."_".date('Y-m-d').'.log');
                }
            }
            
            if($this->config->item('debug')){
                
                error_log("loop : $i |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
            }
            echo "loop : $i |$id ".date('Y-m-d H:i:s')."\n";
            
            //check infobip balance
            $route1Balance=$this->route1_account_balance($id);
            if( $route1Balance >= $this->config->item('route1_minimum_balance')){
                //get active balanced organizations
                $orgs=$this->SMS_model->getActiveOrganizations();

                foreach($orgs AS $key=>$value){

                    if($this->config->item('debug')){

                        error_log("Organization : $value->NAME |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    }
                    echo "Organization : $value->NAME |$id ".date('Y-m-d H:i:s')."\n";

                    //get sms pendings
                    $messages=$this->SMS_model->getMessages('Pending',$id,$this->config->item('route1'),$value->ID);

                    if($messages == NULL){

                        if($this->config->item('debug')){

                            error_log("no pendings |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                        }
                        echo "no pendings |$id ".date('Y-m-d H:i:s')."\n";
                        continue;
                    }

                    $data=array();
                    foreach($messages AS $ky=>$val){

                        if($this->config->item('debug')){

                            error_log("$val->MESSAGEID|$val->MSISDN|$val->SENDERID|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                        }
                        echo "$val->MESSAGEID|$val->MSISDN|$val->SENDERID|$id ".date('Y-m-d H:i:s')."\n";

                        $data[]=array(
                                        "from"=>"$val->SENDERID",
                                        "destinations"=>array(
                                                                array("to"=>"$val->MSISDN","messageId"=>"$val->MESSAGEID")
                                                        ),
                                        "text"=>"$val->SMS",
                                        "transliteration"=>"".$this->config->item('route1_transliteration')."",
                                        "validityPeriod"=>"".$this->config->item('route1_sms_validity_period')."",
                                        "notifyUrl"=>"".$this->config->item('route1_notify_url')."",
                                        "notifyContentType"=>"application/json"
                                    );

                    }

                    $send=$this->sendBulkSmsRoute1(json_encode(array("bulkId"=>time(),"messages"=>$data)),$id);

                    if($send){
                        $dt=array();
                        foreach ($send->messages as $v){

                            $status=$this->SMS_model->getSmsStatus(null,(int)$v->status->groupId);

                            $dt[]=array(
                                'STATUS'=>$status[0]->STATUS,
                                'MESSAGEID'=>$v->messageId,
                                'BULKID'=>$send->bulkId,
                            );
                        }

                        while(TRUE){

                            $sve=$this->SMS_model->save_route1_bulk_data($dt);

                            if($sve){
                                break;
                            }
                        }
                        error_log("complete |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                        echo "complete |$id ".date('Y-m-d H:i:s')."\n";

                    }
                }
            }else{
                
                error_log("route1 balance too low >>> $route1Balance |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                echo "route1 balance too low >>> $route1Balance |$id ".date('Y-m-d H:i:s')."\n";
                continue;
            } 
        }
        
        fclose($lk);
        unlink('./locks/route1_'.$id.'.lock');
    }
    
    /*
     * name : route1_account_balance
     * parameters : id (processing instance)
     * description : retrieve balance with infobip's account
    */
    function route1_account_balance($id){
        $destination="./logs/route1_".date('Y-m-d').'.log';
        
        $encrypt=$this->encrypt_credentials();
        $authorization=$this->config->item('route1_authorization_method').' '.$encrypt;

        $header= array('Content-Type: application/json','Authorization: '.$authorization);
        $body="";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config->item('route1_acc_blnce_url'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response=curl_exec($ch);
        $c_errno=curl_errno($ch);
        $c_err=curl_error($ch);
       
        if($this->config->item('debug')){

            error_log("connection details : ".$c_errno."(".$c_err.") |$id".date('Y-m-d H:i:s')."\n", 3, $destination);
        }
        echo "connection details : ".$c_errno."(".$c_err.") |$id".date('Y-m-d H:i:s')."\n";
        
        curl_close($ch);
        
        if($this->config->item('debug')){

            error_log("response : ".$response." |$id".date('Y-m-d H:i:s')."\n", 3, $destination);
        }
        echo "response : ".$response." |$id".date('Y-m-d H:i:s')."\n";
        
        if($c_errno == 0){
            
            $data=json_decode($response);
            return (string)$data->balance;
        }
        
        return FALSE;
    }
    
    /*
     * name : sendSmsRoute1
     * parameters : sender (sender id),mobile (destination number),msg (actual message),msgId (messageId),id (processing instance)
     * description : infobip sms poster for single recipients
    */
    function sendSmsRoute1($sender,$mobile,$msg,$msgId,$id){
        $destination="./logs/route1_".date('Y-m-d').'.log';
        
        $destiations=array();
        $destiations[]=array("to"=>"$mobile","messageId"=>"$msgId");
        $messages=array();
        $messages[]=array(
                        "from"=>"$sender",
                        "destinations"=>$destiations,
                        "text"=>"$msg",
                        "transliteration"=>"".$this->config->item('route1_transliteration')."",
                        "validityPeriod"=>"".$this->config->item('route1_sms_validity_period')."",
                        "notifyUrl"=>"".$this->config->item('route1_notify_url')."",
                        "notifyContentType"=>"application/json");
        
        $data_array=array("messages"=>$messages);
        
        $data =  json_encode($data_array);
                
        if($this->config->item('debug')){

           error_log("request : ".$data." |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);

        } 
        echo "request : ".$data." |$id ".date('Y-m-d H:i:s')."\n";

        $encrypt=$this->encrypt_credentials();
        $authorization=$this->config->item('route1_authorization_method').' '.$encrypt;

        $header= array('Content-Type: application/json','Authorization: '.$authorization);

        $ch=curl_init($this->config->item('route1_url'));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

        $response=curl_exec($ch);

        if($this->config->item('debug')){

            error_log("connection details : ".curl_errno($ch)."(".curl_error($ch).")|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
            
        }
        echo "connection details : ".curl_errno($ch)."(".curl_error($ch).")|$id ".date('Y-m-d H:i:s')."\n";
        
        curl_close($ch);

        if($this->config->item('debug')){

            error_log("response : ".$response." |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
            
        }
        echo "response : ".$response." |$id ".date('Y-m-d H:i:s')."\n";

        $response=json_decode($response);
        if($response->messages[0] <> null){

            return  $response->messages[0]->status->groupId;
        }
        
        return FALSE;
    }
    
    /*
     * name : sendBulkSmsRoute1
     * parameters : request (data to be send),id (processing instance)
     * description : infobip sms poster for multiple/bulk recipients
    */
    function sendBulkSmsRoute1($request,$id){
        $destination="./logs/route1_".date('Y-m-d').'.log';
         
        error_log("request : ".$request." |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo "request : ".$request." |$id ".date('Y-m-d H:i:s')."\n";

        $encrypt=$this->encrypt_credentials();
        $authorization=$this->config->item('route1_authorization_method').' '.$encrypt;

        $header= array('Content-Type: application/json','Authorization: '.$authorization);

        $ch=curl_init($this->config->item('route1_url'));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

        $response=curl_exec($ch);

        if($this->config->item('debug')){

            error_log("connection details : ".curl_errno($ch)."(".curl_error($ch).")|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
            
        }
        echo "connection details : ".curl_errno($ch)."(".curl_error($ch).")|$id ".date('Y-m-d H:i:s')."\n";
        
        curl_close($ch);

        error_log("response : ".$response." |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
        echo "response : ".$response." |$id ".date('Y-m-d H:i:s')."\n";

        $response=json_decode($response);
        if($response->messages <> null){

            return  $response;
        }
        
        return FALSE;
    }
    
    /*
     * name : route1DlrNotify
     * parameters : none
     * description : infobip sms interface for delivery reports
    */
    function route1DlrNotify(){
        $destination="./logs/route1_dlr_".date('Y-m-d').'.log'; 
        
        $logfile=file_exists($destination);
               
        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 10485760){
                rename($destination,"./logs/route1_dlr_".date('His')."_".date('Y-m-d').'.log');
            }
        }
               
        $request =file_get_contents('php://input');
        error_log("Request : $request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $data=json_decode($request);
       
        $reports=$data->results;
        $dt=array();
        foreach($reports as $value){
            
            $status=$this->SMS_model->getSmsStatus(null,(int)$value->status->groupId);
            
            error_log("BulkId : $value->bulkId|MessageId : $value->messageId|Status Group Id : ".$value->status->groupId."|Status : ".$status[0]->STATUS." ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $dt[]=array(
                'STATUS'=>$status[0]->STATUS,
                'MESSAGEID'=>$value->messageId,
                'BULKID'=>$value->bulkId,
            );
        }
        
        if($dt <> NULL){
            while(TRUE){
               $sve=$this->SMS_model->save_route1_dlr_data($dt);

                if($sve){
                    break;
                } 
            }
            error_log("successfull ".date('Y-m-d H:i:s')."\n", 3, $destination);
        }
        
        error_log("Response : OK ".date('Y-m-d H:i:s')."\n", 3, $destination);
        ob_end_clean();
            
        echo "OK";
        exit;
    }
    
    /*
     * name : encrypt_credentials
     * parameters : none
     * description : infobip suthorization parameter header encryption scheme
    */
    function encrypt_credentials(){
        
        $sec_string=$this->config->item('route1_username').':'.$this->config->item('route1_password');
        $encoded_sec_string=base64_encode($sec_string);
        
        return $encoded_sec_string;
    }
    
/****************************************************************silver street sms functions (route2)****************************************************/
    
    /*
     * name : route2
     * parameters : id (processing instance)
     * description : silverstreet route processing function
    */
    function route2($id){
        $lock =file_exists('./locks/route2_'.$id.'.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/route2_'.$id.'.lock','w'); 
        }
        
        $i=0;
        while($i++ <=$this->config->item('loopcount')){
            $destination="./logs/route2_".date('Y-m-d').'.log';
            $logfile=file_exists($destination);

            if($logfile){

                $filesize=filesize($destination);

                if($filesize > 1048576){
                    rename($destination,"./logs/route2_".date('His')."_".date('Y-m-d').'.log');
                }
            }
            
            if($this->config->item('debug')){
                
                error_log("loop : $i |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
            }
            echo "loop : $i |$id ".date('Y-m-d H:i:s')."\n";
            
            //get active balanced organizations
            $orgs=$this->SMS_model->getActiveOrganizations();
            
            foreach($orgs AS $key=>$value){
                
                if($this->config->item('debug')){
                
                    error_log("Organization : $value->NAME |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                }
                echo "Organization : $value->NAME |$id ".date('Y-m-d H:i:s')."\n";
                
                //get sms pendings
                $messages=$this->SMS_model->getMessages('Pending',$id,$this->config->item('route2'),$value->ID);
                
                if($messages == NULL){
                    
                    if($this->config->item('debug')){
                
                        error_log("no pendings |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    }
                    echo "no pendings |$id ".date('Y-m-d H:i:s')."\n";
                    continue;
                }
                
                $dt=array();
                foreach($messages AS $ky=>$val){
                    
                    if($this->config->item('debug')){
                
                        error_log("$val->MESSAGEID|$val->MSISDN|$val->SENDERID|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    }
                    echo "$val->MESSAGEID|$val->MSISDN|$val->SENDERID|$id ".date('Y-m-d H:i:s')."\n";
                    
                    $data = array(
                            'username' => urlencode($this->config->item('route2_username')),
                            'password' => urlencode($this->config->item('route2_password')),
                            'destination' => urlencode($val->MSISDN),
                            'sender' => urlencode($val->SENDERID),
                            'body' => urlencode($val->SMS),
                            'validity' =>$this->config->item('route2_sms_validity_period'),
                            'bodytype' =>$this->config->item('route2_bodytype'),
                            'reference' => urlencode($val->MESSAGEID),
                            'dlr' => urlencode($this->config->item('route2_dlr_value')),
							'concat' => 1,
                    );
                    
                    $send=$this->sendSmsRoute2($data,$id);
                    
                    if($send){
                        
                        $dt[]=array(
                            'STATUS'=>$send == '01'?'Waiting':'Rejected',
                            'MESSAGEID'=>$val->MESSAGEID
                        );
                    }
                }
                
                if($dt <> NULL){
                    
                    while(TRUE){

                        $sve=$this->SMS_model->save_route2_bulk_data($dt);

                        if($sve){
                            break;
                        }
                    }
                    error_log("complete |$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    echo "complete |$id ".date('Y-m-d H:i:s')."\n";
                }
                
            }
        }
        
        fclose($lk);
        unlink('./locks/route2_'.$id.'.lock');
    }
    
    /*
     * name : sendSmsRoute2
     * parameters : request (data to be send),id (processing instance)
     * description : silverstreet sms poster for single recipients
    */
    function sendSmsRoute2($data,$id){
        $destination="./logs/route2_".date('Y-m-d').'.log';
        
        $request=NULL;
        foreach($data as $key=>$value) {
            
            $request .= $key.'='.$value.'&'; 
            
        }
        
        $request=rtrim($request, '&');
        
        if($this->config->item('debug')){
            
            echo $this->config->item('route2_url')."|$id ".date('Y-m-d H:i:s')."\n";
            error_log($this->config->item('route2_url')."|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
        }
        
        echo "request : $request"."|$id ".date('Y-m-d H:i:s')."\n";
        error_log("request : $request"."|$id ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $ch = curl_init();
        
        curl_setopt($ch,CURLOPT_URL,$this->config->item('route2_url'));
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        
        $response =(string)trim(curl_exec($ch));
        $errno=curl_errno($ch);
        $error=curl_error($ch);
        curl_close($ch);
        
        if($this->config->item('debug')){
            
            echo "$errno|$error|$id ".date('Y-m-d H:i:s')."\n";
            error_log("$errno|$error|$id ".date('Y-m-d H:i:s')."\n",3, $destination);
        }
        
        echo "response : $response |$id ".date('Y-m-d H:i:s')."\n";
        error_log("response : $response |$id ".date('Y-m-d H:i:s')."\n",3, $destination);
        
        if($errno == 0){
            
            return $response;
        }
        return FALSE;
    }
    
    /*
     * name : route2DlrNotify
     * parameters : none
     * description : silverstreet sms interface for delivery reports
    */
    function route2DlrNotify(){
        $destination="./logs/route2_dlr_".date('Y-m-d').'.log'; 
        
        $logfile=file_exists($destination);
               
        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 10485760){
                rename($destination,"./logs/route2_dlr_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $messageID=trim($this->input->get('REFERENCE'));
        $status=trim($this->input->get('STATUS'));
        $reason=trim($this->input->get('REASON'));
        $mobile=trim($this->input->get('DESTINATION'));
        $timestamp=trim($this->input->get('TIMESTAMP'));
        $operator=trim($this->input->get('OPERATOR'));
        
        error_log("Request : ($mobile|$messageID|$status|$timestamp|$operator|$reason) ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $data[]=array(
                    'MESSAGEID'=>$messageID,
                    'STATUS'=>$status,
                    'DELIVERY_TIME'=>$timestamp,
                    'DESTINATION_OPERATOR'=>$operator,
                );
        
        while(TRUE){
            
            $sve=$this->SMS_model->save_route2_dlr_data($data);

            if($sve){
                break;
            } 
        }
        error_log("Response : OK ".date('Y-m-d H:i:s')."\n", 3, $destination);
        ob_end_clean();
            
        echo "OK";
        exit;
    }
}