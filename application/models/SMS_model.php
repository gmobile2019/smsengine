<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SMS_model
 *
 * @author Manuel
 */
class SMS_model extends CI_Model {
    
    private $smsgateway;
    
    function __construct(){
            parent::__construct();
            $this->smsgateway=$this->load->database('smsgateway',TRUE);//load main database configuration
            
    }
    
    function getActiveOrganizations(){
        
        return $this->smsgateway->query("SELECT ID,NAME "
                . "FROM companies "
                . "WHERE STATUS='Active'")->result();
    }
    
    function getMessages($status,$index,$route,$org){
        
        if($status <> NULL){
            
            $where .=" AND STATUS LIKE '$status'";
        }
        
        if($index <> NULL){
            
            $where .=" AND SMSINDEX='$index'";
        }
        
        if($route <> NULL){
            
            $where .=" AND ROUTE='$route'";
        }
        
        if($org <> NULL){
            
            $where .=" AND ORGID='$org'";
        }
        
        return $this->smsgateway->query("SELECT SMS,SENDERID,MSISDN,MESSAGEID "
                . "FROM sms_txns "
                . "WHERE SCHEDULETIME <='".date('Y-m-d H:i:s')."' $where "
                . "ORDER BY ID DESC "
                . "LIMIT 0,".$this->config->item('limitcount'))->result();
    }
    
    function getSmsStatus($id,$groupid){
        
        if($id <> null){
            
            $where .=" AND ID='$id'";
        }
        
        if($groupid <> null){
            
            $where .=" AND INFOBIPGROUPID='$groupid'";
        }
        
        return $this->smsgateway->query("SELECT ID,STATUS,DESCRIPTION,INFOBIPGROUPID FROM sms_status WHERE ID is not null $where")->result();
    }
    
    function save_route1_bulk_data($data){
        
                $this->smsgateway->trans_start();
            foreach($data as $key=>$value){

                $this->smsgateway->query("UPDATE sms_txns SET STATUS='".$value['STATUS']."',BULKID='".$value['BULKID']."' WHERE MESSAGEID='".$value['MESSAGEID']."' AND STATUS='Pending'");
            }
                $this->smsgateway->trans_complete();
        return $this->smsgateway->trans_status();
    }
    
    function save_route2_bulk_data($data){
        
                $this->smsgateway->trans_start();
            foreach($data as $key=>$value){

                $this->smsgateway->query("UPDATE sms_txns SET STATUS='".$value['STATUS']."' WHERE MESSAGEID='".$value['MESSAGEID']."' AND STATUS='Pending'");
            }
                $this->smsgateway->trans_complete();
        return $this->smsgateway->trans_status();
    }
    
    function save_route1_dlr_data($data){
        
                $this->smsgateway->trans_start();
            foreach($data as $key=>$value){

                $this->smsgateway->query("UPDATE sms_txns SET STATUS='".$value['STATUS']."',LASTUPDATE='".date('Y-m-d H;i:s')."' WHERE MESSAGEID='".$value['MESSAGEID']."'");
            }
                $this->smsgateway->trans_complete();
        return $this->smsgateway->trans_status();
    }
    
    function save_route2_dlr_data($data){
        
                $this->smsgateway->trans_start();
            foreach($data as $key=>$value){

                $this->smsgateway->query("UPDATE sms_txns SET STATUS='".$value['STATUS']."',DELIVERY_TIME='".$value['DELIVERY_TIME']."',DESTINATION_OPERATOR='".$value['DESTINATION_OPERATOR']."',LASTUPDATE='".date('Y-m-d H;i:s')."' WHERE MESSAGEID='".$value['MESSAGEID']."'");
            }
                $this->smsgateway->trans_complete();
        return $this->smsgateway->trans_status();
    }
}